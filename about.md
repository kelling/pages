---
layout: page
title: About
permalink: /about/
---

We are a grass-roots movement of Deep Learning practitioners in Dresden, Germany.

## Members of the MLC Organizing Committee

* Falk Zakrzewski
  ([UKD](https://www.uniklinikum-dresden.de/en))
* Heide Meißner
  ([HZDR](https://www.hzdr.de))
* Jeffrey Kelling
  ([HZDR](https://www.hzdr.de))
* Patrick Stiller
  ([HZDR](https://www.hzdr.de))
* Peter Steinbach
  ([MPI-CBG](https://www.mpi-cbg.de), [Scionics](https://www.scionics.de))
* Peter Winkler
  ([TU Dresden, ZIH](https://tu-dresden.de/zih))
* Steffen Seitz
  ([TU Dresden](https://tu-dresden.de/))

### Former Members of the MLC Organizing Committee

* Matthias Werner 
  ([HZDR](https://www.hzdr.de))
